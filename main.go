/**
# Copyright 2015 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
**/

package main

import (
	"flag"
	"fmt"
	"os"
	"html/template"
	"log"
	"net/http"

	"cloud.google.com/go/compute/metadata"
)

type Instance struct {
	Id         string
	Name       string
	Version    string
	Hostname   string
	PHostname   string
	Zone       string
	Project    string
	InternalIP string
	ExternalIP string
	LBRequest  string
	ClientIP   string
	Error      string
}

const version string = "2.0.0"

func main() {
	showversion := flag.Bool("version", false, "display version")
	port := flag.Int("port", 5000, "port to bind")
	flag.Parse()

	if *showversion {
		fmt.Printf("Version %s\n", version)
		return
	}

	http.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) {
		name, err := os.Hostname()
		if nil != err {
			fmt.Fprintf(w, "%s\n", version)
		}
		fmt.Fprintf(w, "%s - %s\n", name, version)
	})

	frontendMode(*port)
	
}

func frontendMode(port int) {

	log.Println("Operating ...")
	tpl := template.Must(template.New("out").Parse(html))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		i := newInstance()
		tpl.Execute(w, i)
	})

	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), nil))
}

type assigner struct {
	err error
}

func (a *assigner) assign(getVal func() (string, error)) string {
	if a.err != nil {
		return ""
	}
	s, err := getVal()
	if err != nil {
		a.err = err
	}
	return s
}

func newInstance() *Instance {
	var i = new(Instance)
	if !metadata.OnGCE() {
		i.Error = "Not running on GCE"
		return i
	}

	a := &assigner{}
	i.Id = a.assign(metadata.InstanceID)
	i.Zone = a.assign(metadata.Zone)
	i.Name = a.assign(metadata.InstanceName)
	i.Hostname = a.assign(metadata.Hostname)
	i.Project = a.assign(metadata.ProjectID)
	i.InternalIP = a.assign(metadata.InternalIP)
	i.ExternalIP = a.assign(metadata.ExternalIP)
	i.Version = version
	if a.err != nil {
		i.Error = a.err.Error()
	}

	name, err := os.Hostname()
	if err != nil {
		i.Error = "Not getting os hostname"
		return i
	}
    i.PHostname = name
	
	return i
}
